package com.linked.usermanage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author :dbq
 * @date : 2022/9/23 16:43
 */
@MapperScan("com.linked.usermanage.*.mapper")
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableAsync
public class UserManage {
    public static void main(String[] args) {
        SpringApplication.run(UserManage.class, args);
    }

}
