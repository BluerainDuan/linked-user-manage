package com.linked.usermanage.provideinterfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.universal.api.UserManageAPI;
import com.linked.commonentity.usermanage.info.LinkedUserInfo;
import com.linked.usermanage.user.bean.po.UserInfoPO;
import com.linked.usermanage.user.service.IUserHandleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/feign")
public class UserManageFeignController implements UserManageAPI {

    private final IUserHandleService userHandleService;

    private final ObjectMapper mapper;

    public UserManageFeignController(IUserHandleService userHandleService, ObjectMapper mapper) {
        this.userHandleService = userHandleService;
        this.mapper = mapper;
    }

    @PostMapping("/findUser")
    public LinkedUserInfo findUser(String userId) {
        UserInfoPO userInfo = userHandleService.findUserInfo(userId);
        LinkedUserInfo linkedUser = null;
        if (!ObjectUtils.isEmpty(userInfo)) {
            try {
                log.info("用户信息：" + mapper.writeValueAsString(userInfo));
                linkedUser = mapper.readValue(mapper.writeValueAsString(userInfo), LinkedUserInfo.class);
            } catch (JsonProcessingException e) {
                log.error("格式转化异常！", e);
            }
        }
        return linkedUser;
    }
}
