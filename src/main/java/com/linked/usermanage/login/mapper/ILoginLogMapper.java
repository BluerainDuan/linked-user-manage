package com.linked.usermanage.login.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.login.bean.po.LoginLogPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :dbq
 * @date : 2022/9/26 10:14
 */
@Mapper
public interface ILoginLogMapper extends BaseMapper<LoginLogPO> {

}
