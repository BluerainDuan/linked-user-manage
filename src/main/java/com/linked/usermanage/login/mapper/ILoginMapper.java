package com.linked.usermanage.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.login.bean.po.LoginPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author :dbq
 * @date : 2022/9/26 9:00
 */
@Mapper
public interface ILoginMapper extends BaseMapper<LoginPO> {

    LoginPO queryPassword(@Param("username") String username);

    int findUsernameUsed(@Param("username") String username);

    LoginPO queryLoginInfoByLoginName(@Param("username") String username);
}
