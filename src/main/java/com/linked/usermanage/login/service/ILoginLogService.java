package com.linked.usermanage.login.service;

import com.linked.usermanage.login.bean.po.LoginLogPO;

/**
 * @author :dbq
 * @date : 2022/9/26 9:47
 */
public interface ILoginLogService {
    void saveLoginLog(LoginLogPO loginLogPO) throws Exception;
}
