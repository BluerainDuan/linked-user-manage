package com.linked.usermanage.login.service.impl;

import com.linked.usermanage.login.bean.po.LoginLogPO;
import com.linked.usermanage.login.mapper.ILoginLogMapper;
import com.linked.usermanage.login.service.ILoginLogService;
import org.springframework.stereotype.Service;

/**
 * @author :dbq
 * @date : 2022/9/26 9:48
 */
@Service
public class LoginLogServiceImpl implements ILoginLogService {

    private final ILoginLogMapper loginLogMapper;

    public LoginLogServiceImpl(ILoginLogMapper loginLogMapper) {
        this.loginLogMapper = loginLogMapper;
    }

    @Override
    public void saveLoginLog(LoginLogPO loginLogPO) throws Exception {
        loginLogMapper.insert(loginLogPO);
    }
}
