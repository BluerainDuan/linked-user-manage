package com.linked.usermanage.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linked.usermanage.login.bean.po.LoginPO;

/**
 * @author :dbq
 * @date : 2022/9/23 16:58
 */
public interface ILoginService {
    /**
     * 查询登录密码接口
     */
    LoginPO queryPassword(String username) throws Exception;

    /**
     * 保存登录信息
     */
    boolean saveLoginInfo(LoginPO loginPO) throws Exception;

    /**
     * 检查用户名是否存在
     */
    boolean checkUsernameUsed(String username) throws Exception;

    /**
     * 获取登录信息
     */
    LoginPO findLoginInfo(String loginName);
}
