package com.linked.usermanage.login.bean.request;

import lombok.Data;

/**
 * @author Bluerain
 * @version 1.0
 * @data 2025/1/8 14:30
 * @describe todo
 */
@Data
public class PassWordUpdateRequest {

    private String loginName;

    private String oldPassWord;

    private String newPassWord;

    private String repartPassWord;


}
