package com.linked.usermanage.login.bean.request;

/**
 * @author :dbq
 * @date : 2022/9/23 17:01
 */
public class LoginRequest {
    private String username;
    private String password;

    private String verifyCode;

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
