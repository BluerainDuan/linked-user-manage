package com.linked.usermanage.login.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.linked.commonentity.usermanage.log.LoginLogEnum;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author :dbq
 * @date : 2022/9/26 9:50
 */
@Data
@TableName("user_login_log")
public class LoginLogPO {

    @TableId(value = "login_log_id", type = IdType.ASSIGN_UUID)
    private String loginLogID;

    @TableField("user_id")
    private String userId;

    @TableField("username")
    private String username;

    @TableField("ip_addr")
    private String ipAddr;

    @TableField("log_type")
    private LoginLogEnum logType;

    @TableField("remarks")
    private Object remarks;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDate createTime;


    public static LoginLogPO Builder(){
        return new LoginLogPO();
    }

    public LoginLogPO withUsername(String username){
        this.setUsername(username);
        return this;
    }
    public LoginLogPO withIPAddr(String ipAddr){
        this.setIpAddr(ipAddr);
        return this;
    }
    public LoginLogPO withRemarks(Object remarks){
        this.setRemarks(remarks);
        return this;
    }

    public LoginLogPO withLogType(LoginLogEnum logType){
        this.setLogType(logType);
        return this;
    }

    public LoginLogPO withUserId(String userId){
        this.setUserId(userId);
        return this;
    }
}
