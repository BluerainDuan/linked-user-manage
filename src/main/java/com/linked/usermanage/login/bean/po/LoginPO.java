package com.linked.usermanage.login.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.linked.usermanage.user.bean.param.UserAndLoginInfoParam;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author :dbq
 * @date : 2022/9/26 8:26
 */
@Data
@TableName("user_login_info")
public class LoginPO {

    @TableId(value = "login_id", type = IdType.ASSIGN_UUID)
    private String loginId;

    @TableField("username")
    private String username;

    @TableField("password")
    private String password;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @TableField("data_status")
    private String dataStatus;

    @TableField("pass_salt")
    private String passSalt;
}
