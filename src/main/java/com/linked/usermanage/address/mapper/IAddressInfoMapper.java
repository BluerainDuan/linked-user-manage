package com.linked.usermanage.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.address.bean.po.AddressInfoPO;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/1/13 9:46
 * @description : desc
 */
@Mapper
public interface IAddressInfoMapper extends BaseMapper<AddressInfoPO> {
    List<UserAddressDTO> queryUserAddressList(@Param("userId") String userId);
}
