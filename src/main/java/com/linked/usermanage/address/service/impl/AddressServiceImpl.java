package com.linked.usermanage.address.service.impl;

import com.linked.usermanage.address.bean.po.AddressInfoPO;
import com.linked.usermanage.address.mapper.IAddressInfoMapper;
import com.linked.usermanage.address.service.IAddressService;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/1/13 9:39
 * @description : desc
 */
@Service
@Slf4j
public class AddressServiceImpl implements IAddressService {

    private final IAddressInfoMapper addressInfoMapper;

    @Autowired
    public AddressServiceImpl(IAddressInfoMapper addressInfoMapper) {
        this.addressInfoMapper = addressInfoMapper;
    }


    @Override
    public boolean saveAddress(AddressInfoPO param) {
        return addressInfoMapper.insert(param) > 0;
    }

    @Override
    public List<UserAddressDTO> queryUserAddressList(String userId) throws Exception {
        return addressInfoMapper.queryUserAddressList(userId);
    }
}
