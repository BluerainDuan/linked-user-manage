package com.linked.usermanage.address.service;

import com.linked.usermanage.address.bean.po.AddressInfoPO;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;

import java.util.List;

/**
 * @author :dbq
 * @date : 2023/1/13 9:39
 * @description : desc
 */
public interface IAddressService {
    boolean saveAddress(AddressInfoPO param) throws Exception;

    List<UserAddressDTO> queryUserAddressList(String userId) throws Exception;
}
