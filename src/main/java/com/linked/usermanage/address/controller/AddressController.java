package com.linked.usermanage.address.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.universal.common.Result;
import com.linked.usermanage.address.bean.po.AddressInfoPO;
import com.linked.usermanage.address.service.IAddressService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author :dbq
 * @date : 2023/1/13 9:28
 * @description : desc
 */
@Slf4j
@RestController
@RequestMapping("address")
public class AddressController {

    private final IAddressService iAddressService;

    private final ObjectMapper mapper;

    @Autowired
    public AddressController(IAddressService iAddressService, ObjectMapper mapper) {
        this.iAddressService = iAddressService;
        this.mapper = mapper;
    }

    @PostMapping("saveAddress")
    Result saveAddress(@NonNull @RequestBody AddressInfoPO param) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("保存用户地址接口，入参：{}", mapper.writeValueAsString(param));
        }
        boolean ret = iAddressService.saveAddress(param);
        return ret ? Result.ok(true, "保存成功！") : Result.ok(false, "保存失败！");
    }


}
