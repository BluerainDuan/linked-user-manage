package com.linked.usermanage.address.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author :dbq
 * @date : 2023/1/13 9:29
 * @description : desc
 */
@Data
@TableName("user_address_info")
public class AddressInfoPO {
    /**
     * 地址表主键
     */
    @TableId(value = "address_id", type = IdType.ASSIGN_UUID)
    private String addressId;
    /**
     * 国家
     */
    @TableField("country")
    private String country;
    /**
     * 省份
     */
    @TableField("province")
    private String province;
    /**
     * 市
     */
    @TableField("city")
    private String city;
    /**
     * 县/区
     */
    @TableField("community")
    private String community;
    /**
     * 街道/乡/镇
     */
    @TableField("township")
    private String township;
    /**
     * 小区/村屯
     */
    @TableField("village")
    private String village;
    /**
     * 详细描述
     */
    @TableField("detail_describe")
    private String detailDescribe;
    /**
     * 坐标
     */
    @TableField("coordinates")
    private String coordinates;
    /**
     * 邮编
     */
    @TableField("zip_code")
    private String zipCode;
    /**
     * 备注
     */
    @TableField("remarks")
    private String remarks;
    /**
     *
     */
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     *
     */
    @TableField("data_status")
    private Integer dataStatus;

    public AddressInfoPO(UserAddressDTO param) {
        this.country = param.getCountry();
        this.province = param.getProvince();
        this.city = param.getCity();
        this.community = param.getCommunity();
        this.township = param.getTownship();
        this.village = param.getVillage();
        this.detailDescribe = param.getDetailDescribe();
        this.coordinates = param.getCoordinates();
        this.zipCode = param.getZipCode();
        this.remarks = param.getRemarks();
    }
}
