package com.linked.usermanage.dept.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.linked.commonentity.usermanage.info.LinkedDeptInfo;
import com.linked.usermanage.dept.bean.param.DeptPageParam;
import com.linked.usermanage.dept.bean.po.DeptInfoPO;
import com.linked.usermanage.dept.bean.vo.DeptPageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IDeptInfoMapper extends BaseMapper<DeptInfoPO> {

    boolean deleteDeptAndSonDept(String deptId);

    List<LinkedDeptInfo> querySonDeptList(String deptId);


    IPage<DeptPageVO> queryDeptPage(DeptPageParam param);
}
