package com.linked.usermanage.dept.bean.vo;

import com.linked.usermanage.dept.bean.po.DeptInfoPO;
import lombok.Data;

@Data
public class DeptPageVO extends DeptInfoPO {
}
