package com.linked.usermanage.dept.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("user_dept_info")
public class DeptInfoPO {

    @TableId(value = "dept_id",type = IdType.ASSIGN_UUID)
    private String deptId;

    @TableField("dept_name")
    private String deptName;

    @TableField("dept_order")
    private Integer deptOrder;

    @TableField("dept_head")
    private String deptHead;

    @TableField("dept_desc")
    private String deptDesc;

    @TableField("pid")
    private String pid;

    @TableField("pids")
    private String pids;

    @TableField("organization_id")
    private String organizationId;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField("data_status")
    private Integer dataStatus;

}
