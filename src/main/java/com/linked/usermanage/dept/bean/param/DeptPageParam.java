package com.linked.usermanage.dept.bean.param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.linked.usermanage.dept.bean.vo.DeptPageVO;
import lombok.Data;

@Data
public class DeptPageParam extends Page<DeptPageVO> {

    private String deptName;

    private String pid;
}
