package com.linked.usermanage.dept.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.linked.universal.common.LinkedPrompt;
import com.linked.universal.common.LinkedResult;
import com.linked.universal.common.Result;
import com.linked.commonentity.usermanage.param.FindDeptParam;
import com.linked.commonentity.usermanage.dto.LinkedDept;
import com.linked.usermanage.dept.bean.param.DeptPageParam;
import com.linked.usermanage.dept.bean.po.DeptInfoPO;
import com.linked.usermanage.dept.bean.vo.DeptPageVO;
import com.linked.usermanage.dept.service.IDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dept")
@Slf4j
public class DeptController {

    private IDeptService deptService;

    private ObjectMapper mapper;

    public DeptController(IDeptService departService, ObjectMapper mapper) {
        this.deptService = departService;
        this.mapper = mapper;
    }

    @PostMapping("/addDept")
    Result addDept(@RequestBody DeptInfoPO param) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("新增部门接口 ----- 入参：{}", mapper.writeValueAsString(param));
        }

        String departId = null;
        try {
            departId = deptService.addDept(param);
        } catch (Exception e) {
            log.error("新增部门接口 ----- 异常！", e);
            return Result.error(LinkedPrompt.ERROR_MESSAGE);
        }
        return Result.success(departId);
    }

    @PostMapping("updateDepart")
    Result updateDepart(@RequestBody DeptInfoPO param) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("修改部门接口 ----- 入参：{}", mapper.writeValueAsString(param));
        }
        boolean ret = false;
        try {
            ret = deptService.updateDepart(param);
        } catch (Exception e) {
            log.error("修改部门接口 ----- 异常！", e);
            return Result.error(LinkedPrompt.ERROR_MESSAGE);
        }
        return Result.success(param);
    }

    @PostMapping("deleteDepart")
    Result deleteDepart(@RequestBody String departId) {
        if (log.isInfoEnabled()) {
            log.info("删除部门接口 ----- 入参：{}" + departId);
        }
        boolean ret = false;
        try {
            ret = deptService.deleteDepart(departId);
        } catch (Exception e) {
            log.error("删除部门接口 ----- 异常！", e);
            return Result.error(LinkedPrompt.ERROR_MESSAGE);
        }
        return Result.success();
    }

    @PostMapping("deleteDeptAndSonDept")
    Result deleteDeptAndSonDept(@RequestBody String departId) {
        if (log.isInfoEnabled()) {
            log.info("删除部门和子部门接口 ----- 入参：{}" + departId);
        }
        boolean ret = false;
        try {
            ret = deptService.deleteDeptAndSonDept(departId);
        } catch (Exception e) {
            log.error("删除部门和子部门接口 ----- 异常！", e);
            return Result.error(LinkedPrompt.ERROR_MESSAGE);
        }
        return Result.success();
    }

    @PostMapping("queryDeptPage")
    Result queryDeptPage(@RequestBody DeptPageParam param) throws Exception {
        if (log.isInfoEnabled()) {
            log.info("查询部门页面接口 ----- 入参：{}", mapper.writeValueAsString(param));
        }
        IPage<DeptPageVO> page = null;
        try {
            page = deptService.queryDeptPage(param);
        } catch (Exception e) {
            log.error("查询部门页面接口 ----- 异常！", e);
            return Result.error(LinkedPrompt.ERROR_MESSAGE);
        }
        return Result.success(page);
    }

    /**
     * 查找部门接口
     */
    @PostMapping("/findDeptTree")
    LinkedResult findDeptTree(@RequestBody FindDeptParam param) {
        LinkedDept deptAll = null;
        try {
            if (log.isInfoEnabled()) {
                log.info("查找部门树接口 ----- 入参：{}", mapper.writeValueAsString(param));
            }
            deptAll = deptService.findDeptTree(param);
        } catch (Exception e) {
            log.error("查找部门树接口 ----- 异常", e);
            return LinkedResult.Error(LinkedPrompt.ERROR_MESSAGE);
        }
        return LinkedResult.Success(deptAll);
    }

    @PostMapping("/findDeptTreeWithUsers")
    LinkedResult findDeptTreeWithUsers(@RequestBody FindDeptParam param) {
        LinkedDept deptAll = null;
        try {
            if (log.isInfoEnabled()) {
                log.info("查找部门树带用户接口 ----- 入参：{}", mapper.writeValueAsString(param));
            }
            deptAll = deptService.findDeptTreeWithUsers(param);
        } catch (Exception e) {
            log.error("查找部门树带用户接口 ----- 异常", e);
            return LinkedResult.Error(LinkedPrompt.ERROR_MESSAGE);
        }
        return LinkedResult.Success(deptAll);
    }
}
