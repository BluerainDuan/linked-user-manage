package com.linked.usermanage.dept.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.linked.commonentity.usermanage.dto.LinkedDept;
import com.linked.commonentity.usermanage.info.LinkedDeptInfo;
import com.linked.commonentity.usermanage.info.LinkedUserInfo;
import com.linked.commonentity.usermanage.param.FindDeptParam;
import com.linked.usermanage.dept.bean.param.DeptPageParam;
import com.linked.usermanage.dept.bean.po.DeptInfoPO;
import com.linked.usermanage.dept.bean.vo.DeptPageVO;
import com.linked.usermanage.dept.mapper.IDeptInfoMapper;
import com.linked.usermanage.dept.service.IDeptService;
import com.linked.usermanage.user.mapper.IUserInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DeptServiceImpl implements IDeptService {

    private final IDeptInfoMapper deptInfoMapper;

    private final IUserInfoMapper userInfoMapper;

    public DeptServiceImpl(IDeptInfoMapper deptInfoMapper, IUserInfoMapper userInfoMapper) {
        this.deptInfoMapper = deptInfoMapper;
        this.userInfoMapper = userInfoMapper;
    }

    @Override
    public String addDept(DeptInfoPO param) throws Exception {
        if (!StringUtils.isEmpty(param.getPid())) {
            DeptInfoPO pDepart = deptInfoMapper.selectById(param.getPid());
            param.setPids(pDepart.getPids() + '|' + pDepart.getDeptId());
        } else {
            //根目录
            param.setPids("1");
        }
        deptInfoMapper.insert(param);
        return param.getDeptId();
    }

    /**
     * 存在问题，修改部门结构时，无法同步部门下
     */
    @Override
    public boolean updateDepart(DeptInfoPO param) throws Exception {
        if (!StringUtils.isEmpty(param.getPid())) {
            DeptInfoPO pDepart = deptInfoMapper.selectById(param.getPid());
            param.setPids(pDepart.getPids() + '|' + pDepart.getDeptId());
        }
        return deptInfoMapper.updateById(param) > 0;
    }

    @Override
    public boolean deleteDepart(String departId) throws Exception {
        return deptInfoMapper.deleteById(departId) > 0;
    }

    @Override
    public boolean deleteDeptAndSonDept(String deptId) throws Exception {
        return deptInfoMapper.deleteDeptAndSonDept(deptId);
    }

    @Override
    public LinkedDept findDeptTree(FindDeptParam param) throws Exception {
        LinkedDept dept = new LinkedDept();

        DeptInfoPO info = deptInfoMapper.selectById(param.getDeptId());
        LinkedDeptInfo deptInfo = new LinkedDeptInfo();
        deptInfo.setDeptId(info.getDeptId());
        deptInfo.setDeptName(info.getDeptName());
        deptInfo.setDeptHead(info.getDeptHead());
        deptInfo.setDeptDesc(info.getDeptDesc());
        deptInfo.setDeptOrder(info.getDeptOrder());
        deptInfo.setPid(info.getPid());
        deptInfo.setPids(info.getPids());
        deptInfo.setOrganizationId(info.getOrganizationId());
        dept.setBaseInfo(deptInfo);

        return buildDeptTree(dept, false);
    }

    private LinkedDept buildDeptTree(LinkedDept info, boolean ifWithUser) throws Exception {

        List<LinkedDept> deptList = new ArrayList<>();
        //1、查询子部门
        List<LinkedDeptInfo> sonDeptList = querySonDeptList(info.getBaseInfo().getDeptId());

        //2、循环子部门
        for (LinkedDeptInfo tmp : sonDeptList) {
            LinkedDept linkedDept = new LinkedDept();
            linkedDept.setBaseInfo(tmp);
            deptList.add(buildDeptTree(linkedDept, ifWithUser));
        }
        info.setSonDeptList(deptList);
        //3、补充人员信息
        if (ifWithUser) {
            List<LinkedUserInfo> userInfoList = userInfoMapper.queryUserListByDeptId(info.getBaseInfo().getDeptId());
            info.setUserList(userInfoList);
        }
        return info;
    }

    private List<LinkedDeptInfo> querySonDeptList(String deptId) {
        return deptInfoMapper.querySonDeptList(deptId);
    }

    @Override
    public LinkedDept findDeptTreeWithUsers(FindDeptParam param) throws Exception {
        LinkedDept dept = new LinkedDept();

        DeptInfoPO info = deptInfoMapper.selectById(param.getDeptId());
        LinkedDeptInfo deptInfo = new LinkedDeptInfo();
        deptInfo.setDeptId(info.getDeptId());
        deptInfo.setDeptName(info.getDeptName());
        deptInfo.setDeptHead(info.getDeptHead());
        deptInfo.setDeptDesc(info.getDeptDesc());
        deptInfo.setDeptOrder(info.getDeptOrder());
        deptInfo.setPid(info.getPid());
        deptInfo.setPids(info.getPids());
        deptInfo.setOrganizationId(info.getOrganizationId());
        dept.setBaseInfo(deptInfo);

        return buildDeptTree(dept, true);
    }

    @Override
    public IPage<DeptPageVO> queryDeptPage(DeptPageParam param) throws Exception {
        return deptInfoMapper.queryDeptPage(param);
    }
}
