package com.linked.usermanage.dept.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.linked.commonentity.usermanage.dto.LinkedDept;
import com.linked.commonentity.usermanage.param.FindDeptParam;
import com.linked.usermanage.dept.bean.param.DeptPageParam;
import com.linked.usermanage.dept.bean.po.DeptInfoPO;
import com.linked.usermanage.dept.bean.vo.DeptPageVO;

public interface IDeptService {
    String addDept(DeptInfoPO param) throws Exception;

    boolean updateDepart(DeptInfoPO param) throws Exception;

    boolean deleteDepart(String departId) throws Exception;

    boolean deleteDeptAndSonDept(String deptId) throws Exception;

    LinkedDept findDeptTree(FindDeptParam param) throws Exception;

    LinkedDept findDeptTreeWithUsers(FindDeptParam param) throws Exception;

    IPage<DeptPageVO> queryDeptPage(DeptPageParam param) throws Exception;
}
