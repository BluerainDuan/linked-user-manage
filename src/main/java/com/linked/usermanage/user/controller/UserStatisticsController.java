package com.linked.usermanage.user.controller;

import com.linked.universal.common.Result;
import com.linked.usermanage.user.bean.dto.UserCountMonthDTO;
import com.linked.usermanage.user.bean.dto.UserRelatedTotalDTO;
import com.linked.usermanage.user.service.IUserHandleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author :dbq
 * @date : 2023/1/16 13:29
 * @description : desc
 */
@Slf4j
@RestController
@RequestMapping("userstatistics")
public class UserStatisticsController {

    private final IUserHandleService userHandleService;

    @Autowired
    public UserStatisticsController(IUserHandleService userHandleService) {
        this.userHandleService = userHandleService;
    }

    /**
     * 查询用户相关总量
     * 1、用户总量
     * 2、本月新增用户量
     * 3、本月用户访问量
     */
    @RequestMapping("queryUserRelatedTotal")
    Result queryUserRelatedTotal() throws Exception {
        UserRelatedTotalDTO res = userHandleService.queryUserRelatedTotal();
        return res != null ? Result.success(res) : Result.ok(false, "未查询到数据");
    }

    /**
     * 查询一年中每个月新增的用户数量
     */
    @PostMapping("queryNewUserCountYear")
    Result queryNewUserCountYear(@NonNull String year) throws Exception {
        List<UserCountMonthDTO> res = userHandleService.queryNewUserCountYear(year);
        return res != null ? Result.success(res) : Result.ok(false, "未查询到数据");
    }

    /**
     * 查询一年中每个月用户登录数量
     */
    @PostMapping("queryUserLoginCountYear")
    Result queryUserLoginCountYear(@NonNull String year) throws Exception {
        List<UserCountMonthDTO> res = userHandleService.queryUserLoginCountYear(year);
        return res != null ? Result.success(res) : Result.ok(false, "未查询到数据");
    }
}
