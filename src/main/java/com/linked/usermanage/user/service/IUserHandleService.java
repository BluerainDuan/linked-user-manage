package com.linked.usermanage.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.linked.usermanage.user.bean.dto.UserAllInfoDTO;
import com.linked.usermanage.user.bean.dto.UserCountMonthDTO;
import com.linked.usermanage.user.bean.dto.UserRelatedTotalDTO;
import com.linked.usermanage.user.bean.param.UserAllInfoParam;
import com.linked.usermanage.user.bean.param.UserContainsRolesParam;
import com.linked.usermanage.user.bean.param.UserPageParam;
import com.linked.usermanage.user.bean.po.UserInfoPO;

import java.util.List;

/**
 * @author :dbq
 * @date : 2022/10/21 14:37
 */
public interface IUserHandleService {

    boolean addAUser(UserInfoPO param) throws Exception;

    IPage<UserInfoPO> queryUserPage(UserPageParam param) throws Exception;

    boolean addRolesToUser(UserContainsRolesParam param) throws Exception;

    UserAllInfoDTO getUserAllInfo(UserAllInfoParam param) throws Exception;

    String queryUserIdByLoginId(String loginId) throws Exception;

    List<UserInfoPO> queryAllUserList() throws Exception;

    UserRelatedTotalDTO queryUserRelatedTotal();

    List<UserCountMonthDTO> queryNewUserCountYear(String year);

    List<UserCountMonthDTO> queryUserLoginCountYear(String year);

    UserInfoPO findUserInfo(String userId);
}
