package com.linked.usermanage.user.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linked.usermanage.address.bean.po.AddressInfoPO;
import com.linked.usermanage.address.mapper.IAddressInfoMapper;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;
import com.linked.usermanage.user.bean.po.UserAddressPO;
import com.linked.usermanage.user.mapper.IUserAddressMapper;
import com.linked.usermanage.user.service.IUserAddressService;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author :dbq
 * @date : 2023/1/13 10:23
 * @description : desc
 */
@Slf4j
@Service
public class UserAddressServiceImpl implements IUserAddressService {

    private final IAddressInfoMapper addressInfoMapper;

    private final IUserAddressMapper userAddressMapper;

    private final ObjectMapper mapper;

    @Autowired
    public UserAddressServiceImpl(IAddressInfoMapper addressInfoMapper, IUserAddressMapper userAddressMapper, ObjectMapper mapper) {
        this.addressInfoMapper = addressInfoMapper;
        this.userAddressMapper = userAddressMapper;
        this.mapper = mapper;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
    @Override
    public boolean saveUserAddress(UserAddressDTO param) throws Exception {

        AddressInfoPO addressInfo = new AddressInfoPO(param);
        int ret1 = addressInfoMapper.insert(addressInfo);
        if (ret1 <= 0) {
            return false;
        }

        param.setAddressId(addressInfo.getAddressId());
        UserAddressPO userAddress = new UserAddressPO(param);
        int ret2 = userAddressMapper.insert(userAddress);
        if (ret2 <= 0) {
            return false;
        }
        return true;
    }
}
