package com.linked.usermanage.user.service;

import com.linked.usermanage.user.bean.dto.UserAddressDTO;

/**
 * @author :dbq
 * @date : 2023/1/13 10:22
 * @description : desc
 */
public interface IUserAddressService {
    boolean saveUserAddress(UserAddressDTO param) throws Exception;
}
