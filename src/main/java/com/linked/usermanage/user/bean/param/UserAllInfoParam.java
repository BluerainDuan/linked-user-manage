package com.linked.usermanage.user.bean.param;

/**
 * @author :dbq
 * @date : 2022/10/21 16:02
 */
public class UserAllInfoParam {
    private String loginId;
    private String userId;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
