package com.linked.usermanage.user.bean.dto;

import lombok.Data;
import org.apache.ibatis.javassist.runtime.Inner;

/**
 * @author :dbq
 * @date : 2023/1/16 14:42
 * @description : desc
 */
@Data
public class UserCountMonthDTO {

    private Integer atMonth;

    private Integer userCount = 0;
}
