package com.linked.usermanage.user.bean.dto;

import lombok.Data;

/**
 * @author :dbq
 * @date : 2023/1/16 13:35
 * @description : desc
 */
@Data
public class UserRelatedTotalDTO {

    /**
     * 用户总量
     */
    private Integer userTotalCount;
    /**
     * 当月新用户量
     */
    private Integer newUserCurrentMonth;
    /**
     * 当月用户访问量
     */
    private Integer userLoginCurrentMonth;

}
