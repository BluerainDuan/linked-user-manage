package com.linked.usermanage.user.bean.param;

import java.util.List;

/**
 * @author :dbq
 * @date : 2022/10/21 15:32
 */
public class UserContainsRolesParam {
    private List<String> roleIds;

    private String userId;

    private String operatorUserId;

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOperatorUserId() {
        return operatorUserId;
    }

    public void setOperatorUserId(String operatorUserId) {
        this.operatorUserId = operatorUserId;
    }
}
