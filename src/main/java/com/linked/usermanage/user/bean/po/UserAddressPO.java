package com.linked.usermanage.user.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.linked.usermanage.user.bean.dto.UserAddressDTO;
import lombok.Data;

/**
 * @author :dbq
 * @date : 2023/1/13 10:31
 * @description : desc
 */
@Data
@TableName("user_user_address")
public class UserAddressPO {

    /**
     *
     */
    @TableId(value = "user_address_id", type = IdType.ASSIGN_UUID)
    private String userAddressId;
    /**
     *
     */
    @TableField("user_id")
    private String userId;
    /**
     *
     */
    @TableField("address_id")
    private String addressId;
    /**
     *
     */
    @TableField("address_type")
    private String addressType;
    /**
     *
     */
    @TableField("if_default")
    private Integer ifDefault;

    public UserAddressPO(UserAddressDTO param) {
        this.userId = param.getUserId();
        this.addressId = param.getAddressId();
        this.ifDefault = param.getIfDefault();
        this.addressType = param.getAddressType();
    }

}
