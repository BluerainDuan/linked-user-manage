package com.linked.usermanage.user.bean.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author :dbq
 * @date : 2023/1/13 10:05
 * @description : desc
 */
@Data
public class UserAddressDTO {

    private String userAddressId;
    private String userId;
    private String addressType;
    private Integer ifDefault;

    /**
     * 地址表主键
     */
    private String addressId;
    /**
     * 国家
     */

    private String country;
    /**
     * 省份
     */

    private String province;
    /**
     * 市
     */

    private String city;
    /**
     * 县/区
     */

    private String community;
    /**
     * 街道/乡/镇
     */

    private String township;
    /**
     * 小区/村屯
     */

    private String village;
    /**
     * 详细描述
     */

    private String detailDescribe;
    /**
     * 坐标
     */

    private String coordinates;
    /**
     * 邮编
     */

    private String zipCode;
    /**
     * 备注
     */
    private String remarks;
    /**
     *
     */

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     *
     */

    private Integer dataStatus;


}
