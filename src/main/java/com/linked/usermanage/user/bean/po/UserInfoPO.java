package com.linked.usermanage.user.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.linked.usermanage.user.bean.param.UserAndLoginInfoParam;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author :dbq
 * @date : 2022/10/21 14:29
 */
@Data
@TableName("user_user_info")
public class UserInfoPO {

    @TableId(value = "user_id", type = IdType.ASSIGN_UUID)
    private String userId;

    @TableField("user_name")
    private String userName;

    @TableField("user_phone")
    private String userPhone;

    @TableField("user_type")
    private Integer userType;

    @TableField("user_position")
    private String userPosition;

    @TableField("sex")
    private Integer sex;

    @TableField("idcard_type")
    private Integer idcardType;

    @TableField("idcard_no")
    private String idcardNo;

    @TableField("language")
    private String language;

    @TableField("country")
    private String country;

    @TableField("login_id")
    private String loginId;

    @TableField("dept_id")
    private String deptId;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField("data_status")
    private Integer dataStatus;

    public UserInfoPO(UserAndLoginInfoParam param) {
        this.country = param.getCountry();
        this.idcardNo = param.getIdcardNo();
        this.idcardType = param.getIdcardType();
        this.userName = param.getUserName();
        this.userPosition = param.getUserPosition();
        this.language = param.getLanguage();
        this.sex = param.getSex();
        this.userType = param.getUserType();
        this.userPhone = param.getUserPhone();

    }

    public UserInfoPO() {
    }
}
