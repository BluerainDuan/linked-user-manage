package com.linked.usermanage.user.bean.dto;

import com.linked.usermanage.user.bean.po.UserInfoPO;

import java.util.List;

/**
 * @author :dbq
 * @date : 2022/10/21 16:09
 */
public class UserAllInfoDTO {

    private UserInfoPO info;




    public UserInfoPO getInfo() {
        return info;
    }

    public void setInfo(UserInfoPO info) {
        this.info = info;
    }


}
