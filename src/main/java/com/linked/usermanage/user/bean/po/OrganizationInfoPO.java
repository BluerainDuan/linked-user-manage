package com.linked.usermanage.user.bean.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("user_organization_info")
public class OrganizationInfoPO {

    @TableId(value = "organization_id",type = IdType.ASSIGN_UUID)
    private String organizationId;

    @TableField("organization_name")
    private String organizationName;

    @TableField("organization_desc")
    private String organizationDesc;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField("data_status")
    private Integer dataStatus;

}
