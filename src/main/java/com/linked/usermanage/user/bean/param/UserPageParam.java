package com.linked.usermanage.user.bean.param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.linked.usermanage.user.bean.po.UserInfoPO;

/**
 * @author :dbq
 * @date : 2022/10/21 14:43
 */
public class UserPageParam extends Page<UserInfoPO> {
}
