package com.linked.usermanage.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.user.bean.po.UserAddressPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :dbq
 * @date : 2023/1/13 10:30
 * @description : desc
 */
@Mapper
public interface IUserAddressMapper extends BaseMapper<UserAddressPO> {
}
