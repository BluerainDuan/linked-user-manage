package com.linked.usermanage.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.user.bean.po.OrganizationInfoPO;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface IOrganizitionInfoMapper extends BaseMapper<OrganizationInfoPO> {
}
