package com.linked.usermanage.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linked.usermanage.user.bean.po.UserRolesPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :dbq
 * @date : 2022/10/21 15:44
 */
@Mapper
public interface IUserRolesMapper extends BaseMapper<UserRolesPO> {
}
