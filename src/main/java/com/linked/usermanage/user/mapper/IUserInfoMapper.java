package com.linked.usermanage.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.linked.commonentity.usermanage.info.LinkedUserInfo;
import com.linked.usermanage.user.bean.dto.UserCountMonthDTO;
import com.linked.usermanage.user.bean.dto.UserRelatedTotalDTO;
import com.linked.usermanage.user.bean.param.UserPageParam;
import com.linked.usermanage.user.bean.po.UserInfoPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author :dbq
 * @date : 2022/10/21 14:39
 */
@Mapper
public interface IUserInfoMapper extends BaseMapper<UserInfoPO> {
    IPage<UserInfoPO> queryUserPage(@Param("param") UserPageParam param);

    List<UserInfoPO> queryAllUserList();

    UserRelatedTotalDTO queryUserRelatedTotal();

    List<UserCountMonthDTO> queryNewUserCountYear(@Param("year") String year);

    List<UserCountMonthDTO> queryUserLoginCountYear(@Param("year") String year);

    List<LinkedUserInfo> queryUserListByDeptId(String deptId);
}
