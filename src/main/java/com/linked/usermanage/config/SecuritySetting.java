package com.linked.usermanage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author :dbq
 * @date : 2022/10/25 15:05
 */
@Configuration
@ConfigurationProperties("securitysetting")
public class SecuritySetting {

    private String pubicKey;

    private String privateKey;

    public String getPubicKey() {
        return pubicKey;
    }

    public void setPubicKey(String pubicKey) {
        this.pubicKey = pubicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
