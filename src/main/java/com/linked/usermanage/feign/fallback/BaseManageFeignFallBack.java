package com.linked.usermanage.feign.fallback;

import com.linked.commonentity.basemanage.deletebak.LinkedDeleteBak;
import com.linked.commonentity.basemanage.generallog.LinkedGeneralLog;
import com.linked.universal.common.LinkedResult;
import com.linked.usermanage.feign.IBaseManageFeign;

/**
 * @author :dbq
 * @date : 2023/1/16 10:16
 * @description : desc
 */
public class BaseManageFeignFallBack implements IBaseManageFeign {
    @Override
    public Boolean findSwitchStatus(String s) {
        return null;
    }

    @Override
    public Boolean turnSwitch(String s) {
        return null;
    }

    @Override
    public void insertGeneralLog(LinkedGeneralLog linkedGeneralLog) {

    }

    @Override
    public LinkedResult queryPassList(Integer integer) throws Exception {
        return null;
    }

    @Override
    public Boolean addDeleteBak(LinkedDeleteBak linkedDeleteBak) {
        return null;
    }
}
