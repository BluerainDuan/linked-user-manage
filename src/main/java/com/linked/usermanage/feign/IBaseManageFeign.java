package com.linked.usermanage.feign;

import com.linked.universal.api.BaseManageAPI;
import com.linked.commonentity.basemanage.pass.param.GeneralLogInfo;
import com.linked.usermanage.feign.fallback.BaseManageFeignFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author :dbq
 * @date : 2023/1/16 10:15
 * @description : desc
 */
@FeignClient(value = "Linked-BaseManage", fallback = BaseManageFeignFallBack.class)
public interface IBaseManageFeign extends BaseManageAPI {


}
